> #***个 人 简 历***

###*个人信息*

------------		
姓名：刘志亮                    
性别：男                     
年龄：26                              
民族：汉                    
籍贯：江西九江                 
学历：本科（2010届毕业生）     
专业：软件工程（嵌入式应用开发）                       
毕业院校：江西农业大学                    
电话：18022480583                    
E-mail：[493735926@qq.com](493735926@qq.com)                    
通讯地址：广州市天河区上社荷光路金古楼B栋
###*技能介绍*								
------------						 	
熟练掌握Objective-C 基础，熟悉Swift语言并使用开发。

熟练掌握MacOSX操作系统Interface Builder， XCode开发环境。

熟练掌握控件的自定义类的封装、数据与界面的交互、内部数据加密的处理，单例模式。

熟练掌握与服务器网络交互，掌握使用ASIHttpRequest、AFNetworking。

熟练掌握使用JSONKit，FMDB，SDWebImage,CocoaAsyncSocket等其他第三方库

熟悉ios的推送机制

熟练掌握C语言。

###*工作经历*		

------------														 	
* ######公司名称：北京中科长青科技有限公司
1. 时间：2009年11月28日-2010年5月28日
2. 职位：嵌入式软件工程师
3. 工作任务：按照公司电路图焊小型开发板，改写公司模块程序以及在开发板上测试。

* ######公司名称：广州海悦科技发展有限公司
1. 时间：2010年6月21日-2011年5月18日
2. 职位：嵌入式软件工程师
3. 工作任务：主要从事linux下C/C++编程。在OPENGL框架下，PVR平台下使用PVRShell引擎，使用C/C++语言开发2D游戏，并移植到arm板运行。裁减烧写omap arm板angstrom系统，测试arm板系统以致稳定工作。

* ######公司名称：广东汇卡商务服务有限公司
1. 时间：2011年5月23日-2012年3月
2. 职位：iOS软件工程师
3. 工作职责：从事公司金掌柜产品的iphone客户端开发。Qt开发。

* ######公司名称：广东亿迅科技有限公司
1. 时间：2012年3月12日-至今
2. 职位：iOS软件工程师
3. 工作职责：从事公司iphone客户端开发。

###*工作项目经历*		

-----------														 	
* ######维护ARM板系统（2010年6月到2011年5月）
1. 环境：ARM Linux系统
2. 裁剪ARM板系统，添加模块。并加入程序运行的库文件，关于opengl方面的库。最后烧写到arm板，实现程序的稳定运行。编写linux脚本，实现程序的运行和配置文件的修改。后期系统的维护。
* ######万能金鲨游戏、世界杯、极品飞车、吃火锅（纸牌类游戏）、开心农场单双人台(后台可调)（2010年11月-2011年5月）
1. 环境：ubuntu，c/c++，按键板，codelite，vi，Makefile，G++
2. 项目负责人，负责项目的整体开发。与美工的交流游戏界面的设计，达到与程序完整体现。代码逻辑实现前台界面的表现，并与后台交换数据，实现后台数据的存储。并实现一些用户操作。前台实现玩家的投币，退币，压分和界面的表现。后期与测试部门交流，修改BUG。
* ######金掌柜iphone客户端
1. 环境：xcode4.2, iOS SDK.
2. 负责该项目的整体开发。UI设计开发，控件的自定义类的封装、内部数据加密的处理和数据与界面的交互、以及与服务器通讯。
* ######集中收银和发卡子系统
1. 环境：qt SDK. C/C++
2. 项目的UI设计，内部数据加密处理。以及调用DLL库，来与服务器进行通讯，以及数据与界面的交互。
* ######翼校通、家校微博、易企通、翼赢销、CC2013等iPhone客户端开发（2012.3-至今）
1. 环境：xcode, iOS SDK.
2. 相关技术：控件的自定义类的封装、数据与界面的交互、内部数据加密的处理、推送机制。ASIHttpRequest, AFNetworking,JSONKit,GCD,FMDB,SDWebImage,CocoaAsyncSocket,以及其他第三方库
3. iPhone客户端开发主要负责人。参与需求的确定，项目的整体规划，代码的整理，后期的维护，以及任务的分配
                                                                                              
### *自我评价*							

--------					 	
本人具有良好的学习能力，团队意识强，爱好广泛，人际关系良好，待人诚恳，具有吃苦耐劳、艰苦奋斗的精神；工作认真负责，积极主动，喜欢冷静思考问题，适应能力强，较强的组织协调能力，富有创新精神。

### *作品*

翼赢销 ： [http://61.140.99.50:9010/html/download.html](http://61.140.99.50:9010/html/download.html)

 

[https://itunes.apple.com/us/app/ai-xue-xi-men-hu-shi-pin-zhong/id734319085?ls=1&mt=8](https://itunes.apple.com/us/app/ai-xue-xi-men-hu-shi-pin-zhong/id734319085?ls=1&mt=8)

[https://itunes.apple.com/us/app/fu-jian-jiao-yu-yun/id778244897?ls=1&mt=8](https://itunes.apple.com/us/app/fu-jian-jiao-yu-yun/id778244897?ls=1&mt=8)

 

[https://itunes.apple.com/us/app/yi-qi-tong2.0/id541494898?ls=1&mt=8](https://itunes.apple.com/us/app/yi-qi-tong2.0/id541494898?ls=1&mt=8) （后期没人维护，已下架）


CC2013：

 

[http://tytx.chinatelecom.com.cn/](http://tytx.chinatelecom.com.cn/)

[https://dl.dropboxusercontent.com/s/8jh7vh8d2jjbx0w/CC2013install.html](https://dl.dropboxusercontent.com/s/8jh7vh8d2jjbx0w/CC2013install.html)